package feed;

import java.util.*;
import javax.servlet.http.*;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import feed.Parameter;

/**
 * 
 * @author Daniel Kadner
 * @version 1.0
 *
 */
public class FeedContent {
	public final static String FILE = "uploadedfiles[]";
	public static String EXECUTABLE = "executable";
	public static String NUMBEROFPARAMS = "numberofparams";
	public static String CONTAINERTYPE = "cont_type_select";
	public static String PROCESSID = "processid";
	public static String PROCESSTITLE = "processtitle";
	public static String PROCESSABSTRACT = "processabstract";
	public static String PARAM = "param_";
	public static String RUNTIME = "Run_Env_";
	
	public static String ID = "_id";
	public static String PREFIX = "_prefix";
	public static String SUFFIX = "_suffix";
	public static String SEPARATOR = "_separator";
	public static String TITLE = "_title";
	public static String ABSTRACT = "_abstract";
	public static String FORMAT = "_format";
	public static String MINIMUM = "_minimum";
	public static String MAXIMUM = "_maximum";
	public static String KIND = "_kind";
	

	private FileItem file;
	private int numberOfParams = 0;
	private ArrayList<Parameter> params;
	private String containerType = "";
	private ArrayList<RequiredBackend> requiredBackends;
	private String p_id = "";
	private String p_title = "";
	private String p_abstract = "";
	
	/**
	 * create a FeedContent structure from request
	 * @param request the {@link HttpServletRequest}
	 */
	@SuppressWarnings("unchecked")
	public FeedContent(HttpServletRequest request) {
		try {
			if (request != null) {
				requiredBackends = new ArrayList<RequiredBackend>();
				params = new ArrayList<Parameter>();
				if (ServletFileUpload.isMultipartContent(request)) {
					ServletFileUpload sfu = new ServletFileUpload(new DiskFileItemFactory());
					List<FileItem> fil = sfu.parseRequest(request);
					Iterator<FileItem> it = fil.iterator();
					while (it.hasNext()) {
						FileItem fi = (FileItem) it.next();
						if (fi.isFormField()) {

							//read out selected container type
							if (fi.getFieldName().equalsIgnoreCase(CONTAINERTYPE)) {
								this.containerType = fi.getString();
							}
							
							//read out selected runtime Environments
							else if (fi.getFieldName().matches(RUNTIME+"\\d+-\\d+_select")) {
								String index = fi.getFieldName().substring(RUNTIME.length(), fi.getFieldName().indexOf("-"));
								if (containsBackend(index) == -1){
									RequiredBackend rb = new RequiredBackend(index);
									requiredBackends.add(rb);
								}
								addRequiredBackend(index, fi.getString());
							}
							
							//read out number of params
							else if (fi.getFieldName().equalsIgnoreCase(NUMBEROFPARAMS)) {
								if (!(fi.getString().equals(""))) {
									this.numberOfParams = Integer.parseInt(fi.getString());
								}
							} 
							
							//read out params
							else if (fi.getFieldName().startsWith(PARAM)) {
								
								String index = fi.getFieldName().substring(PARAM.length(), fi.getFieldName().indexOf("-"));
								
								if (containsParam(index) == -1){
									params.add(new Parameter(index));
								}
								Parameter p = getParam(index);
								if (p != null){
									if (fi.getFieldName().endsWith(ID)){
										p.setProcessId(fi.getString());
									} else if (fi.getFieldName().endsWith(PREFIX)){
										p.setPrefix(fi.getString());
									}  else if (fi.getFieldName().endsWith(SUFFIX)){
										p.setSuffix(fi.getString());
									} else if (fi.getFieldName().endsWith(SEPARATOR)){
										p.setSeparator(fi.getString());
									} else if (fi.getFieldName().endsWith(TITLE)){
										p.setTitle(fi.getString());
									} else if (fi.getFieldName().endsWith(ABSTRACT)){
										p.setAbstract(fi.getString());
									} else if (fi.getFieldName().endsWith(FORMAT)){
										p.addFormat(fi.getString());
									} else if (fi.getFieldName().endsWith(MINIMUM)){
										p.setMinimum(fi.getString());
									} else if (fi.getFieldName().endsWith(MAXIMUM)){
										p.setMaximum(fi.getString());
									} else if (fi.getFieldName().endsWith(KIND)){
										if (fi.getString().equals("input"))
											p.setKind(true);
										else 
											p.setKind(false);
									}
								}
							}  
							
							//read out id of process
							else if (fi.getFieldName().equalsIgnoreCase(PROCESSID)){
								this.p_id = fi.getString();
							}
							
							//read out title of process
							else if (fi.getFieldName().equalsIgnoreCase(PROCESSTITLE)){
								this.p_title = fi.getString();
							}
							
							//read out abstract of process
							else if (fi.getFieldName().equalsIgnoreCase(PROCESSABSTRACT)){
								this.p_abstract = fi.getString();
							}
						} else {
							if (fi.getFieldName().equalsIgnoreCase(FILE)) {
								this.file = fi;
							}
						}
					}
				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return the unique id
	 */
	public String getId() {
		return p_id;
	}

	/**
	 * set the unique id
	 * @param id
	 */
	public void setId(String id) {
		this.p_id = id;
	}

	/**
	 * 
	 * @return the submitted file
	 */
	public FileItem getFile() {
		return file;
	}

	/**
	 * set the submitted file
	 * @param file
	 */
	public void setFile(FileItem file) {
		this.file = file;
	}

	/**
	 * 
	 * @return the number of parameters for this FeedContent
	 */
	public int getNumberOfParams() {
		return numberOfParams;
	}

	/**
	 * set the number of parameters for this FeedContent
	 * @param numberOfParams
	 */
	public void setNumberOfParams(int numberOfParams) {
		this.numberOfParams = numberOfParams;
	}

	/**
	 * 
	 * @return the parameters as list
	 */
	public ArrayList<Parameter> getParams() {
		return params;
	}

	/**
	 * set the list of parameters
	 * @param params
	 */
	public void setParams(ArrayList<Parameter> params) {
		this.params = params;
	}

	/**
	 * 
	 * @return the type of the container
	 */
	public String getContainerType() {
		return containerType;
	}

	/**
	 * set the type of the container
	 * @param containerType
	 */
	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}

	/**
	 * 
	 * @return the list of required backends
	 */
	public ArrayList<RequiredBackend> getRequiredBackends() {
		return requiredBackends;
	}

	/**
	 * set the list of required backends
	 * @param requiredBackends
	 */
	public void setRequiredBackends(ArrayList<RequiredBackend> requiredBackends) {
		this.requiredBackends = requiredBackends;
	}
	
	private int containsBackend(String id){
		for (int i = 0; i < requiredBackends.size(); i++ ){
			if (requiredBackends.get(i).getId().equals(id)) return i;
		}
		return -1;
	}
	
	private void addRequiredBackend(String index, String backend) {
		for (int i = 0; i < requiredBackends.size(); i++ ){
			if (requiredBackends.get(i).getId().equals(index)) {
				requiredBackends.get(i).addBackend(backend);
			}
		}
	}
	
	private int containsParam(String id){
		for (int i = 0; i < params.size(); i++ ){
			if (params.get(i).getInternalID().equals(id)) return i;
		}
		return -1;
	}
	
	private Parameter getParam(String index) {
		for (int i = 0; i < params.size(); i++ ){
			if (params.get(i).getInternalID().equals(index)) {
				return params.get(i);
			}
		}
		return null;
	}

	/**
	 * 
	 * @return title of the process
	 */
	public String getProcessTitle() {
		return p_title;
	}

	/**
	 * set the title of the process
	 * @param title
	 */
	public void setProcessTitle(String title) {
		this.p_title = title;
	}

	/**
	 * 
	 * @return the absract of the process
	 */
	public String getProcessAbstract() {
		return p_abstract;
	}

	/**
	 * set the abstract of the process
	 * @param abstract_
	 */
	public void setProcessAbstract(String abstract_) {
		this.p_abstract = abstract_;
	}
	
	/**
	 * create a FeedContent filled with values for testing
	 */
	//for testing purpose
	public FeedContent(){
		requiredBackends = new ArrayList<RequiredBackend>();
		params = new ArrayList<Parameter>();
		this.setId("de.tu-dresden.geo.gis.algorithms.raster.ztransform");
		this.setProcessTitle("z-Transformation for raster data (cell wise)");
		this.setProcessAbstract("Computes a z-transform cell by cell. Image coordinates, geo-references and projections of both rasters MUST match!");
		this.setContainerType("urn:n52:container:python:2.5");
		RequiredBackend rb1 = new RequiredBackend("1");
		rb1.addBackend("urn:n52:lib:python:2.6");
		rb1.addBackend("urn:n52:lib:gdal:1.8");
		requiredBackends.add(rb1);
		RequiredBackend rb2 = new RequiredBackend("2");
		rb2.addBackend("urn:n52:lib:python:2.5");
		rb2.addBackend("urn:n52:lib:gdal:1.8");
		requiredBackends.add(rb2);
		Parameter p1 = new Parameter("STATS_RASTER");
		p1.setPrefix("");
		p1.setSuffix("");
		p1.setSeparator(" ");
		p1.setTitle("Statistics Rasters");
		p1.setAbstract("collection of single band rasters to compute a reference sigma and mean from");
		p1.addFormat("application/geotiff");
		p1.setMinimum("2");
		p1.setMaximum("1000");
		params.add(p1);
		
		Parameter p2 = new Parameter("REFERENCE_RASTER");
		p2.setPrefix("");
		p2.setSuffix("");
		p2.setSeparator("");
		p2.setTitle("Reference Raster X");
		p2.setAbstract("A single band raster that shall be z-transformed");
		p2.addFormat("application/geotiff");
		p2.setMinimum("1");
		p2.setMaximum("1");
		params.add(p2);
		
		Parameter p3 = new Parameter("ZTRANSFORM");
		p3.setPrefix("");
		p3.setSuffix("");
		p3.setSeparator("");
		p3.setTitle("z-transformed reference raster X");
		p3.setAbstract("a z-transformed single band raster");
		p3.addFormat("application/geotiff");
		p3.setMinimum("");
		p3.setMaximum("");
		params.add(p3);
	}
}