package feed;

import java.util.LinkedList;

/**
 * Class representing a Parameter 
 * 
 * @author Daniel Kadner
 * @version 1.0
 *
 */
public class Parameter {
	
	private String internal_id;
	private String process_id;
	private String input;
	private String output;
	private String prefix;
	private String suffix;
	private String separator;
	private String title;
	private String abstract_;
	private LinkedList<String> format;
	private String minimum;
	private String maximum;
	//true: input, false: output
	private boolean kind;

	/**
	 * Constructor 
	 * @param id the internal id
	 */
	public Parameter (String id ){
		this.internal_id = id;
	}

	/**
	 * 
	 * @return the process id of this parameter
	 */
	public String getProcessId() {
		return process_id;
	}
	
	/**
	 * set the process id of this parameter
	 * @param id a unique id
	 */
	public void setProcessId(String id) {
		this.process_id = id;
	}
	
	/**
	 * 
	 * @return the internal id of the parameter
	 */
	public String getInternalID(){
		return this.internal_id;
	}
	
	/**
	 * set the internal id
	 * @param id a unique value
	 */
	public void setInternalID(String id){
		this.internal_id = id;
	}

	/**
	 * 
	 * @return the input of this parameter
	 */
	public String getInput() {
		return input;
	}

	/**
	 * set the input name of this parameter
	 * @param input
	 */
	public void setInput(String input) {
		this.input = input;
	}

	/**
	 * 
	 * @return the output name of this parameter
	 */
	public String getOutput() {
		return output;
	}

	/**
	 * set the output name of this parameter
	 * @param output
	 */
	public void setOutput(String output) {
		this.output = output;
	}
	
	/**
	 * 
	 * @return the prefix of this parameter
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * set the prefix of this parameter
	 * @param prefix
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * 
	 * @return the suffix of this parameter
	 */
	public String getSuffix() {
		return suffix;
	}

	/**
	 * set the suffix of this parameter
	 * @param suffix
	 */
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	/**
	 * 
	 * @return the separator of this parameter
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 * set the separator of this parameter
	 * @param separator
	 */
	public void setSeparator(String separator) {
		this.separator = separator;
	}
	
	/**
	 * 
	 * @return the title of this parameter
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * set the title of this parameter
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @return the abstract of this parameter
	 */
	public String getAbstract() {
		return abstract_;
	}

	/**
	 * set the abstract of this parameter
	 * @param abstract_
	 */
	public void setAbstract(String abstract_) {
		this.abstract_ = abstract_;
	}

	/**
	 * 
	 * @return a list of formats of this parameter
	 */
	public LinkedList<String> getFormat() {
		return format;
	}

	/**
	 * set the list of formats of this parameter
	 * @param format
	 */
	public void setFormat(LinkedList<String> format) {
		this.format = format;
	}
	
	/**
	 * add a new format to the list of formats of this parameter
	 * @param newformat
	 */
	public void addFormat(String newformat){
		if (this.format == null){
			this.format = new LinkedList<String>();
		}
		this.format.add(newformat);
	}

	/**
	 * 
	 * @return the minimum appearance of this parameter
	 */
	public String getMinimum() {
		return minimum;
	}

	/**
	 * set the minimum appearance of this parameter
	 * @param minimum
	 */
	public void setMinimum(String minimum) {
		this.minimum = minimum;
	}

	/**
	 * 
	 * @return the maximum appearance of this parameter
	 */
	public String getMaximum() {
		return maximum;
	}

	/**
	 * set the maxmimum appearance of this parameter
	 * @param maximum
	 */
	public void setMaximum(String maximum) {
		this.maximum = maximum;
	}
	
	/**
	 * 
	 * @return a string representing if parameter is input or output
	 */
	public boolean getKind(){
		return kind;
	}
	
	/**
	 * set the kind of this parameter
	 * @param kind possible values are "input" or "output"
	 */
	public void setKind(boolean kind){
		this.kind = kind;
	}
}
