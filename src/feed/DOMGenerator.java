package feed;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Comment;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * 
 * @author Daniel Kadner
 * @version 1.0
 *
 */
public class DOMGenerator {

	private FeedContent fc;
	private Document doc;

	public DOMGenerator(FeedContent fc) {
		this.fc = fc;
		createXMLDOM();
		printXMLDOM();
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args){
		DOMGenerator dom = new DOMGenerator(createTestFeedContent());
	}

	/**
	 * create the document structure
	 */
	public void createXMLDOM() {
		try {
			// We need a Document
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			DOMImplementation domImpl = docBuilder.getDOMImplementation();
			
			doc = domImpl.createDocument("http://gis.geo.tu-dresden.de/movingcode/1.0.0", "mvc:packageDescription", null);

			// //////////////////////
			// Creating the XML tree
			
			// create the root element and add it to the document
			
			
			Element root = doc.getDocumentElement();
			root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:ows", "http://www.opengis.net/ows/1.1");
			root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:wps", "http://www.opengis.net/wps/1.0.0");
			root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
			root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xml", "http://www.w3.org/XML/1998/namespace");
			root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			root.setAttributeNS("http://www.w3.org/2001/XMLSchema-instance", "xsi:schemaLocation", "http://gis.geo.tu-dresden.de/movingcode/1.0.0 MovingCodePackage.xsd");
			
			//****************************
			// workspace
			//****************************
			
			Element mvc_workspaceDescription = doc.createElement("mvc:workspaceDescription");
			root.appendChild(mvc_workspaceDescription);
			
			Element mvc_workspaceRoot = doc.createElement("mvc:workspaceRoot");
			mvc_workspaceRoot.setTextContent("./"+fc.getFile().getName().substring(0, fc.getFile().getName().indexOf(".")));
			mvc_workspaceDescription.appendChild(mvc_workspaceRoot);
			
			Element mvc_executableLocation = doc.createElement("mvc:executableLocation");
			mvc_executableLocation.setTextContent("./"+fc.getFile().getName());
			mvc_workspaceDescription.appendChild(mvc_executableLocation);
			
			Element mvc_containerType = doc.createElement("mvc:containerType");
			mvc_containerType.setTextContent(fc.getContainerType());
			mvc_workspaceDescription.appendChild(mvc_containerType);
			
			for (RequiredBackend be : fc.getRequiredBackends()){
				Element mvc_validRuntimeEnvironment = doc.createElement("mvc:validRuntimeEnvironment");
				for (String runEnv: be.getBackends()){
					Element mvc_requiredRuntimeComponent = doc.createElement("mvc:requiredRuntimeComponent");
					mvc_requiredRuntimeComponent.setTextContent(runEnv);
					mvc_validRuntimeEnvironment.appendChild(mvc_requiredRuntimeComponent);
				}
				mvc_workspaceDescription.appendChild(mvc_validRuntimeEnvironment);
			}
			
			Element mvc_executionParameters = doc.createElement("mvc:executionParameters");
			mvc_executionParameters.setAttribute("mvc:sequential", "true");
			
			//inputs
			Comment inputComment = doc.createComment("Inputs");
			mvc_executionParameters.appendChild(inputComment);
			
			for(Parameter p: fc.getParams()){
				if (p.getKind()){
					
					Element mvc_parameter = doc.createElement("mvc:parameter");
										
					Element mvc_prefixString = doc.createElement("mvc:prefixString");
					mvc_prefixString.setTextContent(p.getPrefix());
					mvc_parameter.appendChild(mvc_prefixString);
					
					Element mvc_suffixString = doc.createElement("mvc:suffixString");
					mvc_suffixString.setTextContent(p.getSuffix());
					mvc_parameter.appendChild(mvc_suffixString);
					
					Element mvc_separatorString = doc.createElement("mvc:separatorString");
					mvc_separatorString.setTextContent(p.getSeparator());
					mvc_parameter.appendChild(mvc_separatorString);
					
					Element mvc_positionID = doc.createElement("mvc:positionID");
					mvc_positionID.setTextContent(p.getInternalID());
					mvc_parameter.appendChild(mvc_positionID);
					
					Element mvc_functionalInputID = doc.createElement("mvc:functionalInputID");
					mvc_functionalInputID.setTextContent(p.getProcessId());
					mvc_parameter.appendChild(mvc_functionalInputID);
					
					Element mvc_functionalOutputID = doc.createElement("mvc:functionalOutputID");
					mvc_parameter.appendChild(mvc_functionalOutputID);
					
					mvc_executionParameters.appendChild(mvc_parameter);
				}
			}
			
			//outputs
			Comment outputComment = doc.createComment("Outputs");
			mvc_executionParameters.appendChild(outputComment);
			
			for(Parameter p: fc.getParams()){
				if (!p.getKind()){
					
					Element mvc_parameter = doc.createElement("mvc:parameter");
										
					Element mvc_prefixString = doc.createElement("mvc:prefixString");
					mvc_prefixString.setTextContent(p.getPrefix());
					mvc_parameter.appendChild(mvc_prefixString);
					
					Element mvc_suffixString = doc.createElement("mvc:suffixString");
					mvc_suffixString.setTextContent(p.getSuffix());
					mvc_parameter.appendChild(mvc_suffixString);
					
					Element mvc_separatorString = doc.createElement("mvc:separatorString");
					mvc_separatorString.setTextContent(p.getSeparator());
					mvc_parameter.appendChild(mvc_separatorString);
					
					Element mvc_positionID = doc.createElement("mvc:positionID");
					mvc_positionID.setTextContent(p.getInternalID());
					mvc_parameter.appendChild(mvc_positionID);
					
					Element mvc_functionalInputID = doc.createElement("mvc:functionalInputID");
					mvc_parameter.appendChild(mvc_functionalInputID);
					
					Element mvc_functionalOutputID = doc.createElement("mvc:functionalOutputID");
					mvc_functionalInputID.setTextContent(p.getProcessId());
					mvc_parameter.appendChild(mvc_functionalOutputID);
					
					mvc_executionParameters.appendChild(mvc_parameter);
				}
			}
			
			mvc_workspaceDescription.appendChild(mvc_executionParameters);
			
			//****************************
			// functional
			//****************************
			
			Element mvc_functionalDescription = doc.createElement("mvc:functionalDescriptions");
			root.appendChild(mvc_functionalDescription);
			
			Element mvc_wpsProcessDescription = doc.createElement("mvc:wpsProcessDescription");
			mvc_wpsProcessDescription.setAttribute("wps:processVersion", "1");
			mvc_functionalDescription.appendChild(mvc_wpsProcessDescription);
			
			Element ows_Identifier = doc.createElement("ows:Identifier");
			ows_Identifier.setTextContent(fc.getId());
			mvc_wpsProcessDescription.appendChild(ows_Identifier);
			
			Element ows_Title = doc.createElement("ows:Title");
			ows_Title.setTextContent(fc.getProcessTitle());
			mvc_wpsProcessDescription.appendChild(ows_Title);
			
			Element ows_Abstract = doc.createElement("ows:Abstract");
			ows_Abstract.setTextContent(fc.getProcessAbstract());
			mvc_wpsProcessDescription.appendChild(ows_Abstract);
			
			Element dataInputs = doc.createElement("DataInputs");
			mvc_wpsProcessDescription.appendChild(dataInputs);
			
			for(Parameter p: fc.getParams()){
				if (p.getKind()){
					
					Element input = doc.createElement("Input");
					if (p.getMinimum() != null && !p.getMinimum().equals(""))
						input.setAttribute("minOccurs", p.getMinimum());
					if(p.getMaximum() != null && !p.getMaximum().equals(""))
						input.setAttribute("maxOccurs", p.getMaximum());
					dataInputs.appendChild(input);
					
					Element ows_param_Identifier = doc.createElement("ows:Identifier");
					ows_param_Identifier.setTextContent(p.getProcessId());
					input.appendChild(ows_param_Identifier);
					
					Element ows_param_Title = doc.createElement("ows:Title");
					ows_param_Title.setTextContent(p.getTitle());
					input.appendChild(ows_param_Title);
					
					Element ows_param_Abstract = doc.createElement("ows:Abstract");
					ows_param_Abstract.setTextContent(p.getAbstract());
					input.appendChild(ows_param_Abstract);
					
					Element complexData = doc.createElement("ComplexData");
					input.appendChild(complexData);
					
					Element default_ = doc.createElement("Default");
					complexData.appendChild(default_);
					
					Element format_def = doc.createElement("Format");
					default_.appendChild(format_def);
					
					Element mimeType_def = doc.createElement("MimeType");
					mimeType_def.setTextContent(p.getFormat().getFirst());
					format_def.appendChild(mimeType_def);
					
					Element schema_def = doc.createElement("Schema");
					format_def.appendChild(schema_def);
					
					Element supported = doc.createElement("Supported");
					complexData.appendChild(supported);
					
					for (String mime: p.getFormat()){
						Element format_sup = doc.createElement("Format");
						supported.appendChild(format_sup);
						
						Element mimeType_sup = doc.createElement("MimeType");
						mimeType_sup.setTextContent(mime);
						format_sup.appendChild(mimeType_sup);
						
						Element schema_sup = doc.createElement("Schema");
						format_sup.appendChild(schema_sup);
					}
					
				}
			}
			
			Element processOutputs = doc.createElement("ProcessOutputs");
			mvc_wpsProcessDescription.appendChild(processOutputs);
			
			for(Parameter p: fc.getParams()){
				if (!p.getKind()){
					
					Element output = doc.createElement("Output");
					processOutputs.appendChild(output);
					
					Element ows_param_Identifier = doc.createElement("ows:Identifier");
					ows_param_Identifier.setTextContent(p.getProcessId());
					output.appendChild(ows_param_Identifier);
					
					Element ows_param_Title = doc.createElement("ows:Title");
					ows_param_Title.setTextContent(p.getTitle());
					output.appendChild(ows_param_Title);
					
					Element ows_param_Abstract = doc.createElement("ows:Abstract");
					ows_param_Abstract.setTextContent(p.getAbstract());
					output.appendChild(ows_param_Abstract);
					
					Element complexOutput = doc.createElement("ComplexOutput");
					output.appendChild(complexOutput);
					
					Element default_ = doc.createElement("Default");
					complexOutput.appendChild(default_);
					
					Element format_def = doc.createElement("Format");
					default_.appendChild(format_def);
					
					Element mimeType_def = doc.createElement("MimeType");
					mimeType_def.setTextContent(p.getFormat().getFirst());
					format_def.appendChild(mimeType_def);
					
					Element schema_def = doc.createElement("Schema");
					format_def.appendChild(schema_def);
					
					Element supported = doc.createElement("Supported");
					complexOutput.appendChild(supported);
					
					for (String mime: p.getFormat()){
						Element format_sup = doc.createElement("Format");
						supported.appendChild(format_sup);
						
						Element mimeType_sup = doc.createElement("MimeType");
						mimeType_sup.setTextContent(mime);
						format_sup.appendChild(mimeType_sup);
						
						Element schema_sup = doc.createElement("Schema");
						format_sup.appendChild(schema_sup);
					}
				}
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * print the document as xml on standard output
	 */
	public void printXMLDOM() {
		try {
			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");
			trans.setOutputProperty(OutputKeys.VERSION, "1.0");
			trans.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

			// create string from xml tree
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
			String xmlString = sw.toString();

			// print xml
			System.out.println("Here's the xml:\n\n" + xmlString);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return the document as xml file
	 */
	public File asXML(){
		try {
			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");
			trans.setOutputProperty(OutputKeys.VERSION, "1.0");
			trans.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

			// create string from xml tree
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
			String xmlString = sw.toString();

			File file = new File(fc.getId() + ".xml");
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
			bw.write(xmlString);
			bw.flush();
			bw.close();
			
			return file;
			
			// print xml
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private static FeedContent createTestFeedContent(){
		return new FeedContent();
	}
}
