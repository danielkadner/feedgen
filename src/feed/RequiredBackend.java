package feed;

import java.util.ArrayList;

/**
 * Handles the required backends from the request
 * 
 * @author Daniel Kadner
 * @version 1.0
 */
public class RequiredBackend {

	private String id;
	private ArrayList<String> backends;
	
	/**
	 * Constructor, requires id
	 * @param id a unique id for the required Backend
	 */
	public RequiredBackend(String id){
		this.id = id;
	}

	/**
	 * 
	 * @return the id for this RequiredBackend
	 */
	public String getId() {
		return id;
	}

	/**
	 * set the id
	 * @param id a unique id for the rquired Backend
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 
	 * @return list which cotains the required Backends as Strings
	 */
	public ArrayList<String> getBackends() {
		return backends;
	}
	/**
	 * 
	 * @param backends ArrayList with the required Backends
	 */
	public void setBackends(ArrayList<String> backends) {
		this.backends = backends;
	}
	
	/**
	 * add a new required Backend to the list of required Backends
	 * @param newBackend the new backend which will be added
	 */
	public void addBackend(String newBackend){
		if (this.backends == null){
			backends = new ArrayList<String>();
		}
		backends.add(newBackend);
	}
}