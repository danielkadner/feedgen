package feed;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;

/**
 * Class for converting the request into mvc package (zip file)
 * @author Daniel Kadner
 * @version 1.0
 *
 */
public class FeedGenerator {
	private HttpServletRequest request;
	private FeedContent fc;
	// private File xslt;
	private File zipFile;
	DOMGenerator dom = null;
	

	private static final int BUFFER = 2048;

	/**
	 * Construtor, using the request and create {@link FeedContent}
	 * @param request
	 */
	public FeedGenerator(HttpServletRequest request) {
		this.request = request;
	
		fc = new FeedContent(request);
		if (fc.getFile() != null) {
			dom = new DOMGenerator(fc);
		}

		if (dom != null) {
			createZIP();
		}
	}

	/**
	 * to check, whether the {@link DOMGenerator} is created or not 
	 * @return true if the document is created, false otherwise
	 */
	public boolean isCreated() {
		boolean result = true;
		if (dom == null) {
			result = false;
		}
		return result;
	}

	/**
	 * create the mvc-package structure and zip it
	 * @return true if mvc package was succesfully build, false otherwise
	 */
	private boolean createZIP() {
		try {
			File tempfolder = createTempDir(fc.getId());
			File subfolder = new File(tempfolder, fc.getId());
			subfolder.mkdir();
			copyFile(subfolder, fc.getFile());
			copyFile(tempfolder, dom.asXML());
			copyFile(tempfolder, request.getServletContext().getResourceAsStream("/WEB-INF/MovingCodePackage.xsd"), "MovingCodePackage.xsd");

			zipFile = File.createTempFile(fc.getId(), ".zip");

			zipDir(zipFile, tempfolder);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * copy a fileitem to a folder
	 * @param folder the folder where the fileitem have to be coppied in
	 * @param fi the fileitem to copy
	 */
	public static void copyFile(File folder, FileItem fi) {
		try {
			File f = new File(folder, fi.getName());
			f.createNewFile();
			InputStream inputStream = fi.getInputStream();
			OutputStream out = new FileOutputStream(f);
			byte buf[] = new byte[BUFFER];
			int len;
			while ((len = inputStream.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * copy a file to a folder
	 * @param folder the folder where the file have to be coppied in
	 * @param file the file to copy
	 */
	public static void copyFile(File folder, File file) {
		try {

			File f = new File(folder, file.getName());
			f.createNewFile();
			InputStream inputStream = new FileInputStream(file);
			OutputStream out = new FileOutputStream(f);
			byte buf[] = new byte[BUFFER];
			int len;
			while ((len = inputStream.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * copy a file to a folder by using the inputstring and a filename
	 * @param folder where the new file should be added
	 * @param is the inputstream of the new file
	 * @param filename the name for the new file
	 */
	public static void copyFile(File folder, InputStream is, String filename) {
		try {
			File f = new File(folder, filename);
			f.createNewFile();
			OutputStream out = new FileOutputStream(f);
			byte buf[] = new byte[BUFFER];
			int len;
			while ((len = is.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * create a temporaly folder 
	 * @param foldername the name of the folder
	 * @return the temporaly create folder as file
	 */
	public static File createTempDir(String foldername) {
		final String baseTempPath = System.getProperty("java.io.tmpdir");
		Calendar now = Calendar.getInstance();

		File tempDir = new File(baseTempPath + File.separator + foldername + "_" + now.getTimeInMillis());
		if (tempDir.exists() == false) {
			tempDir.mkdir();
		}

		tempDir.deleteOnExit();

		return tempDir;
	}

	/**
	 * create a zip file from a given folder
	 * @param zipFileName the zip file's name
	 * @param dir the folder to zip
	 * @throws Exception if param "dir" is not been found
	 */
	private static void zipDir(File zipFileName, File dir) throws Exception {
		File dirObj = dir;
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
		addDir(dirObj, out, dir);
		out.close();
	}

	/**
	 * add an existing folder to another existing folder
	 * @param dirObj the existing folder to copy
	 * @param out
	 * @param base
	 * @throws IOException
	 */
	public static void addDir(File dirObj, ZipOutputStream out, File base) throws IOException {
		File[] files = dirObj.listFiles();
		byte[] tmpBuf = new byte[BUFFER];

		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				addDir(files[i], out, base);
				continue;
			}
			FileInputStream in = new FileInputStream(files[i].getAbsolutePath());
			out.putNextEntry(new ZipEntry(relative(base, files[i])));
			int len;
			while ((len = in.read(tmpBuf)) > 0) {
				out.write(tmpBuf, 0, len);
			}
			out.closeEntry();
			in.close();
		}
	}

	/**
	 * create a relative file name 
	 * @param base the base file
	 * @param file the target file
	 * @return relative filename representing file(name) - base(name)
	 */
	public static String relative(final File base, final File file) {
		final int rootLength = base.getAbsolutePath().length();
		final String absFileName = file.getAbsolutePath();
		final String relFileName = absFileName.substring(rootLength + 1);
		return relFileName;
	}

	/**
	 * 
	 * @return the zipped mvc package file 
	 */
	public File getZipFile() {
		return zipFile;
	}

//	public static void main(String[] args) {
//
//	}

}
