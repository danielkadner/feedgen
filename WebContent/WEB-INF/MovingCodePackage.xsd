<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Description Schema for Moving Code Packages
Author: Matthias Mueller, TU Dresden
Website: http://tu-dresden.de/fgh/geo/gis

Version: 1.0.0
-->

<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:wps100="http://www.opengis.net/wps/1.0.0" targetNamespace="http://gis.geo.tu-dresden.de/movingcode/1.0.0" xmlns:mvc="http://gis.geo.tu-dresden.de/movingcode/1.0.0" elementFormDefault="qualified" attributeFormDefault="qualified">
	
	<xs:import namespace="http://www.opengis.net/wps/1.0.0" schemaLocation="http://schemas.opengis.net/wps/1.0.0/wpsDescribeProcess_response.xsd"/>
	
	<xs:element name="packageDescription">
		<xs:annotation>
				<xs:documentation>Deployment Description for a Moving Code Package. Contains platform specific information and the functional description.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element xmlns="mvc" name="workspaceDescription" type="mvc:workspaceDescriptionType">
					<xs:annotation>
						<xs:documentation>Structure describing the non-functional part of the moving code package. Contains platform-specific information and parameter mapping to the functional description.</xs:documentation>
					</xs:annotation>				
				</xs:element >
				<xs:element xmlns="mvc" name="functionalDescriptions" type="mvc:functionalDescriptionCollectionType">
					<xs:annotation>
						<xs:documentation>Structure describing the functional part of the moving code package. Contains functional parameters whose Identifiers must match with the parameters in the non-functional description.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<!-- ========================================================================== -->
	<xs:complexType xmlns="mvc" name="functionalDescriptionCollectionType">
		<xs:annotation>
			<xs:documentation>
				A list of supported functional descriptions.
				Multiple descriptions are possible; they represent different views on the same execution logic.
				Currently implements only a WPS 1.0.0 process description, but can be extended by other functional description types.
				I/O Identifiers used in the functional description need to match those in the parameter mapping in the workspace description.
				Identifiers for the provided logic MUST be identical in all functional descriptions.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element xmlns="mvc" name="wpsProcessDescription" type="wps100:ProcessDescriptionType" minOccurs="0" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Structure describing the functional part of the moving code package. Based on WPS 1.0.0 specification</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<!-- ========================================================================== -->		
	<xs:complexType xmlns="mvc" name="workspaceDescriptionType">
		<xs:annotation>
			<xs:documentation>Deployment Description for a Moving Code Package. Contains platform specific information and the functional description.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="workspaceRoot" type="xs:anyURI">
				<xs:annotation>
					<xs:documentation>workspace location relative to the package root, e.g. "../scriptworkspace"</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="executableLocation" type="xs:anyURI">
				<xs:annotation>
					<xs:documentation>location of the executable; relative to the workspace root. If appropriate, this location can point to some location within the container, e.g. "../toolbox.tbx#toolset1/toolA"</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="containerType" type="xs:anyURI">
				<xs:annotation>
					<xs:documentation>URN referencing the specific container, e.g. urn:n52:wps:algorithmcontainer:arctoolbox:9.3</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="validRuntimeEnvironment" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>A list of valid runtime configurations for this executable.</xs:documentation>
				</xs:annotation>	
				<xs:complexType>
					<xs:sequence>
						<xs:element name="requiredRuntimeComponent" type="xs:anyURI" maxOccurs="unbounded">
							<xs:annotation>
								<xs:documentation>URN referencing a required processing backend, e.g. urn:n52:wps:gpsystem:arcgis:9.3</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="executionParameters">
				<xs:annotation>
					<xs:documentation>A collection of the execution parameters</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="parameter" type="mvc:ExecutionParameterType" maxOccurs="unbounded">
							<xs:annotation>
								<xs:documentation>The tool's parameters</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element name="separatorString" type="xs:string" minOccurs="0">
							<xs:annotation>
								<xs:documentation>A string that acts as a seperator between parameters, e.g. " " for SPACE</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
					<xs:attribute name="sequential" type="xs:boolean">
						<xs:annotation>
							<xs:documentation>If true, this tool's parameters are indexed through their position as integers. Otherwise, the mapping from the ows:Identifier to the respective LegacyID is done via name strings.</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<!-- ========================================================================== -->
	<xs:complexType xmlns="mvc" name="ExecutionParameterType">
		<xs:annotation>
			<xs:documentation>Structure for a parameter description.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="prefixString" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>A prefix string that shall be added before this parameter, e.g. "-p("</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="suffixString" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>A suffix string that shall be added before this parameter, e.g. ")"</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="separatorString" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>A string that acts as a separator between individual parameter values, e.g. " " for SPACE</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:choice>
				<xs:annotation>
					<xs:documentation>The Legacy ID by which the parameters are defined in the Legacy environment. Use legacyIntID for sequential parameters, legacyStrindID for named parameters</xs:documentation>
				</xs:annotation>
				<xs:element name="positionID" type="mvc:PositionIDType"/>
				<xs:element name="stringID" type="mvc:StringIDType"/>
			</xs:choice>
			<xs:element name="functionalInputID" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The WPS Input ID this parameter shall be mapped to.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="functionalOutputID" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The WPS Output ID this parameter shall be mapped to.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<!-- ========================================================================== -->
	<xs:simpleType xmlns="mvc" name="PositionIDType">
		<xs:annotation>
			<xs:documentation>Positive integer indicating the position of this parameter.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:positiveInteger"/>
	</xs:simpleType>
	<!-- ========================================================================== -->
	<xs:simpleType xmlns="mvc" name="StringIDType">
		<xs:annotation>
			<xs:documentation>String indicating the name of this parameter.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string"/>
	</xs:simpleType>
</xs:schema>
