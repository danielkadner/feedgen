dojo.addOnLoad(function() {

});
function setRadioButtons(paramID, elementid) {
	dojo.create("input", {
		type : "radio",
		id : paramID + "_Input",
		value : "in"
	}, elementid);
	new dijit.form.RadioButton({
		onChange : function() {
			if(this.get('checked')) {
				var last_ = this.id.lastIndexOf('_');
				var currentID = this.id.substring(0, last_);
				dojo.byId(currentID + "_kind").value = "input";
				var tabName = dijit.byId(currentID + "_id").get('value');
				if(tabName === "") {
					last_ = currentID.lastIndexOf('-');
					var currentID_ = currentID.substring(last_, currentID.length);
					tabName = "ID" + currentID_;
					dijit.byId(currentID + "_id").set('value', tabName);
				}

				var newTab = true;
				var children = dijit.byId("output_tab_Container").getChildren();
				for(var i = 0; i < children.length; i++) {
					if(children[i].get('title') === tabName) {
						dijit.byId("output_tab_Container").removeChild(children[i]);
						dijit.byId("input_tab_Container").addChild(children[i]);
						newTab = false;
					}
				}
				if(newTab) {
					var newIn;
					//erzeuge neuen, wenn die is im output noch nicht vorhanden
					newIn = new dijit.layout.ContentPane({
						id : paramID + "_Input_Tab",
						title : tabName,
						content : createTabContent(currentID)
					});
					dijit.byId("input_tab_Container").addChild(newIn);
				}
				
				dojo.byId(currentID + "_minMaxDiv").style.visibility = 'visible';
			}
		},
		name : paramID
	}, paramID + "_Input");
	dojo.create("label", {
		"for" : paramID + "_Input",
		innerHTML : "Input"
	}, elementid);

	dojo.create("input", {
		type : "radio",
		id : paramID + "_Output",
		value : "out",
	}, elementid);
	new dijit.form.RadioButton({
		onChange : function() {
			if(this.get('checked')) {
				var last_ = this.id.lastIndexOf('_');
				var currentID = this.id.substring(0, last_);
				dojo.byId(currentID + "_kind").value = "output";
				var tabName = dijit.byId(currentID + "_id").get('value');
				if(tabName === "") {
					last_ = currentID.lastIndexOf('-');
					var currentID_ = currentID.substring(last_, currentID.length);
					tabName = "ID" + currentID_;
					dijit.byId(currentID + "_id").set('value', tabName);
				}

				var newTab = true;
				//lösche erst den alten tab aus dem input
				var children = dijit.byId("input_tab_Container").getChildren();
				for(var i = 0; i < children.length; i++) {
					if(children[i].get('title') === tabName) {
						dijit.byId("input_tab_Container").removeChild(children[i]);
						dijit.byId("output_tab_Container").addChild(children[i]);
						newTab = false;
					}
				}
				//erzeuge neuen tab
				if(newTab) {
					var newOut = new dijit.layout.ContentPane({
						id : paramID + "_Output_Tab",
						title : tabName,
						content : createTabContent(currentID)
					});
					dijit.byId("output_tab_Container").addChild(newOut);
				}

				//zeige minimum, maximum
				dojo.byId(currentID + "_minMaxDiv").style.visibility = 'hidden';
			}
		},
		name : paramID
	}, paramID + "_Output");
	dojo.create("label", {
		"for" : paramID + "_Output",
		innerHTML : "Output"
	}, elementid);
}

function clearInputOutputTab() {
	var children = dijit.byId("input_tab_Container").getChildren();
	if(children != undefined) {
		for(var i = 0; i < children.length; i++) {
			dojo.destroy(children[i]);
			dijit.byId("input_tab_Container").closeChild(children[i]);
		}
	}

	var children = dijit.byId("output_tab_Container").getChildren();
	if(children != undefined) {
		for(var i = 0; i < children.length; i++) {
			dojo.destroy(children[i]);
			dijit.byId("output_tab_Container").closeChild(children[i]);
		}
	}
}

function createTabContent(hid) {
	var tab_content = dojo.create("div");

	//title
	dojo.create("label", {
		"for" : hid + "_title",
		innerHTML : "Title:"

	}, tab_content);
	dojo.create("br", null, tab_content);
	var paramTITLEHelper = dojo.create("div", null, tab_content);
	new dijit.form.TextBox({
		id : hid + "_title",
		"name" : hid + "_title",
		type : "text",
		placeHolder : "the title of the parameter",
		style : {
			width : "300px"
		}
	}, paramTITLEHelper);
	dojo.create("br", null, tab_content);

	//abstract
	dojo.create("label", {
		"for" : hid + "_abstract",
		innerHTML : "Abstract:"

	}, tab_content);
	dojo.create("br", null, tab_content);
	var paramABSTRACTHelper = dojo.create("div", null, tab_content);
	new dijit.form.SimpleTextarea({
		id : hid + "_abstract",
		"name" : hid + "_abstract",
		value : "an abstract for the parameter",
		type : "text",
		placeHolder : "an abstract for the parameter",
		style : {
			"min-width" : "300px",
			"max-width" : "300px"
		}
	}, paramABSTRACTHelper);
	dojo.create("br", null, tab_content);

	//mimetype
	var currentID = hid + "_" + getId();

	dojo.create("label", {
		"for" : currentID + "_format",
		innerHTML : "Format:"
	}, tab_content);
	dojo.create("br", null, tab_content);
	var format_div_helper = dojo.create("div", null, tab_content);
	createFormatBox(currentID + "_format", format_div_helper);
	dojo.create("br", null, tab_content);

	dojo.create("div", {
		id : hid + "_formatadd"
	}, tab_content);
	dojo.create("img", {
		src : "images/add.png",
		width : "16",
		height : "16",
		alt : "Add",
		onClick : "addFormat(\'" + hid + "\')"
	}, tab_content);
	dojo.create("br", null, tab_content);

	var paramMINMAXDIVHelper = dojo.create("div", {
		id : hid + "_minMaxDiv"
	}, tab_content);

	//minimum
	dojo.create("label", {
		"for" : hid + "_minimum",
		innerHTML : "Minimum amount of arguments:"
	}, paramMINMAXDIVHelper);
	dojo.create("br", null, paramMINMAXDIVHelper);
	var paramMINIMUMHelper = dojo.create("div", null, paramMINMAXDIVHelper);
	new dijit.form.NumberSpinner({
		id : hid + "_minimum",
		"name" : hid + "_minimum",
		smallDelta : 1,
		intermediateChanges : true,
		constraints : {
			min : 0,
			max : 99999
		},
		style : {
			width : "40px"
		},
		onChange : function() {
			if(dijit.byId(hid + "_minimum").get('value') > dijit.byId(hid + "_maximum").get('value')) {
				dijit.byId(hid + "_maximum").set('value', dijit.byId(hid + "_minimum").get('value'));
			}
		}
	}, paramMINIMUMHelper);
	dojo.create("br", null, paramMINMAXDIVHelper);

	//maximum
	dojo.create("label", {
		"for" : hid + "_maximum",
		innerHTML : "Maximum amount of arguments:"

	}, paramMINMAXDIVHelper);
	dojo.create("br", null, paramMINMAXDIVHelper);
	var paramMAXIMUMHelper = dojo.create("div", null, paramMINMAXDIVHelper);
	new dijit.form.NumberSpinner({
		id : hid + "_maximum",
		"name" : hid + "_maximum",
		smallDelta : 1,
		intermediateChanges : true,
		value : 2,
		constraints : {
			min : 2
		},
		style : {
			width : "40px"
		},
		onChange : function() {
			if(dijit.byId(hid + "_maximum").get('value') < dijit.byId(hid + "_minimum").get('value')) {
				dijit.byId(hid + "_minimum").set('value', dijit.byId(hid + "_maximum").get('value'));
			}
		}
	}, paramMAXIMUMHelper);
	dojo.create("br", null, paramMINMAXDIVHelper);

	//maximum disablen, wenn kein separator vorhanden
	if(dijit.byId(hid + "_separator").get('value') === "") {
		dijit.byId(hid + "_minimum").set('value', 0);
		dijit.byId(hid + "_minimum").constraints.min = 0;
		dijit.byId(hid + "_minimum").constraints.max = 1;
		// dijit.byId(hid + "_minimum").set('disabled', true);
		dijit.byId(hid + "_maximum").set('value', 1);
		dijit.byId(hid + "_maximum").set('disabled', true);

	}
	return tab_content;
}

function createFormatBox(hid, place) {

	var mime_Types_Store = new dojo.data.ItemFileReadStore({
		url : "json/Mime_Types.json"
	});

	new dijit.form.FilteringSelect({
		id : hid,
		name : hid,
		value : "application/geotiff",
		store : mime_Types_Store,
		searchAttr : "mimetype",
		style : {
			width : "300px"
		}
	}, place);
}

function removeFormat(format) {
	dojo.destroy(format);
}

function addFormat(id) {
	var newId = id + "_" + getId();

	var format = dojo.create("div", {
		id : newId
	}, id + "_formatadd", "before");
	
	var format_add_helper_span = dojo.create("span");
	dojo.place(format_add_helper_span, format);
	
	var format_box = createFormatBox(newId + "_select", format_add_helper_span);
	var format_remove = dojo.create("img", {
		src : "images/del.png",
		width : "16",
		height : "16",
		alt : "Remove",
		onClick : "removeFormat(\'" + newId + "\')"
	}, format);
}