var tab_name = 1;

dojo.addOnLoad(function() {

});
function tab_run_env_onClick(tab) {
	if(tab.selectedChildWidget.id === "tab_plus") {
		var currentID = getId();
		var newTab = new dijit.layout.ContentPane({
			id : "Run_Env_" + currentID,
			title : tab_name,
			closable : true,
			onClose : function() {
				var tab_name_new = 1;
				for(var i = 0; i < tab.getChildren().length; i++) {
					currentTab = tab.getChildren()[i];
					if(currentTab.title !== this.title && currentTab.id !== "tab_plus") {
						currentTab.set("title", tab_name_new);
						tab_name_new++;
					}
				}
				tab_name = tab_name_new;
				return true;
			},
			content : createRunEnv("Run_Env_" + currentID)
		});
		tab.addChild(newTab, tab.getChildren().length - 1);
		tab.selectChild(newTab);
		tab_name++;
	}
}

function createRunEnv(id_) {
	var currentID = id_ + "-" + getId();
	var run_env_div = dojo.create("div");
	var run_env = dojo.create("div", {
		id : currentID
	}, run_env_div);

	createRunEnvBox(currentID + "_select", run_env);

	var run_env_div_add = dojo.create("div", {
		id : id_ + "_add"
	}, run_env_div);
	dojo.create("img", {
		src : "images/add.png",
		width : "16",
		height : "16",
		alt : "Add",
		onClick : "addRequiredBackend(\'" + id_ + "\')"
	}, run_env_div_add);

	return run_env_div;
}

function createRunEnvBox(hid, place) {

	var required_Backends_Store = new dojo.data.ItemFileReadStore({
		url : "json/Required_Backends.json"
	});

	var filteringSelect = new dijit.form.FilteringSelect({
		id : hid,
		name : hid,
		value : "urn:n52:lib:python:2.5",
		store : required_Backends_Store,
		searchAttr : "backend",
		style : {
			width : "300px"
		}
	}, place);
}

function removeRequiredBackend(requiredbackend) {
	dojo.destroy(requiredbackend);
}

function addRequiredBackend(id) {
	var newId = id + "-" + getId();

	var run_env = dojo.create("div", {
		id : newId
	}, id + "_add", "before");

	var run_env_add_helper_span = dojo.create("span");
	dojo.place(run_env_add_helper_span, run_env);
	createRunEnvBox(newId + "_select", run_env_add_helper_span);
	var run_env_remove = dojo.create("img", {
		src : "images/del.png",
		width : "16",
		height : "16",
		alt : "Remove",
		onClick : "removeRequiredBackend(\'" + newId + "\')"
	}, run_env);
}