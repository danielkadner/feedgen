dojo.addOnLoad(function() {
	//boolean for showing the infobutton
	// var b_infoField = false;
	var numberspinner = new dijit.form.NumberSpinner({
		id : "numberofparams",
		name : "numberofparams",
		value: 1,
		smallDelta : 1,
		intermediateChanges : true,
		constraints : {
			min : 1,
			max : 99
		},
		style : {
			width : "40px"
		},
		onChange : function() {
			// if(!b_infoField){
				// dojo.style("infofield", "display", "inline");
				// b_infoField = true;
			// }
			setParams(this.value);
			clearInputOutputTab();
		}
	},"numberofparams_content");
	setParams(1);
	
	dojo.connect(dojo.byId("infofield"), "onclick", dijit.byId("dialogColor"), "show");
	
	//setup the dialog interaction
	dojo.connect(dojo.byId("colorprefix"), "onmouseover", function(){
		dojo.style('coloredprefix','fontWeight','bold');
		dojo.style('coloredprefix','backgroundColor', '#ccc');
	});
	dojo.connect(dojo.byId("colorprefix"), "onmouseout", function(){
		dojo.style('coloredprefix','fontWeight','normal');	
		dojo.style('coloredprefix','backgroundColor', '#fff');
	});
	
	dojo.connect(dojo.byId("colorseparator"), "onmouseover", function(){
		dojo.style('coloredseparator1','fontWeight','bold');
		dojo.style('coloredseparator2','fontWeight','bold');
		dojo.style('coloredseparator1','backgroundColor', '#ccc');
		dojo.style('coloredseparator2','backgroundColor', '#ccc');	
	});
	dojo.connect(dojo.byId("colorseparator"), "onmouseout", function(){
		dojo.style('coloredseparator1','fontWeight','normal');
		dojo.style('coloredseparator2','fontWeight','normal');
		dojo.style('coloredseparator1','backgroundColor', '#fff');
		dojo.style('coloredseparator2','backgroundColor', '#fff');		
	});
	
	dojo.connect(dojo.byId("colorsuffix"), "onmouseover", function(){
		dojo.style('coloredsuffix','fontWeight','bold');
		dojo.style('coloredsuffix','backgroundColor', '#ccc');	
	});
	dojo.connect(dojo.byId("colorsuffix"), "onmouseout", function(){
		dojo.style('coloredsuffix','fontWeight','normal');	
		dojo.style('coloredsuffix','backgroundColor', '#fff');	
	});
});

function setParams(num) {

	var divParams = dojo.byId("params");
	dojo.empty("params");

	for(var i = 1; i <= num; i++) {
		var newpreid = "param_" + i + "-" + getId();
		var param_div = dojo.create("div", {
			id : newpreid
		}, divParams);
		dojo.create("label", {
			innerHTML : "Parameter " + i
		}, param_div);

		var param_inner = dojo.create("div", {
			"class" : "innerparams"
		}, param_div);

		//input/output
		dojo.create("label", {
			"for" : "param_" + i + "_inout",
			"class" : "innerparam_label"
		}, param_inner);
		var paramINOUTHelper = dojo.create("span", {
			id : "param_" + i + "_inout"
		}, param_inner);
		setRadioButtons(newpreid, paramINOUTHelper);
		
		var paramHIDDENHelper = dojo.create("span", null, param_inner);
		new dijit.form.TextBox({
			id : newpreid + "_kind",
			"name" : newpreid + "_kind",
			type : "hidden"
		}, paramHIDDENHelper);
		
		dojo.create("br", null, param_inner);

		//id
		dojo.create("label", {
			"for" : newpreid + "_id",
			"class" : "innerparam_label",
			innerHTML : "id:"
		}, param_inner);
		var paramIDHelper = dojo.create("div", null, param_inner);
		new dijit.form.ValidationTextBox({
			id : newpreid + "_id",
			"name" : newpreid + "_id",
			type : "text",
			intermediateChanges : true,
			required : true,
			onChange : function() {
				var last_ = this.id.lastIndexOf('_');
				var currentID = this.id.substring(0, last_);
				var tabName = dijit.byId(currentID + "_id").get('value');
				//check which radio box is active
				var children;
				if(dijit.byId(currentID + "_Input").get('checked')) {
					children = dijit.byId("input_tab_Container").getChildren();
					for(var i = 0; i < children.length; i++) {
						if(children[i].get('id') === currentID + "_Input_Tab" || children[i].get('id') === currentID + "_Output_Tab") {
							children[i].set('title', tabName);
						}
					}
				}
				if(dijit.byId(currentID + "_Output").get('checked')) {
					
					children = dijit.byId("output_tab_Container").getChildren();
					for(var i = 0; i < children.length; i++) {
						if(children[i].get('id') === currentID + "_Output_Tab"  || children[i].get('id') === currentID + "_Input_Tab") {
							children[i].set('title', tabName);
						}
					}
				}
			}
		}, paramIDHelper);

		dojo.create("br", null, param_inner);
		

		//prefix
		dojo.create("label", {
			"for" : newpreid + "_prefix",
			"class" : "innerparam_label",
			innerHTML : "prefix:"
		}, param_inner);
		var paramPREHelper = dojo.create("div", null, param_inner);
		new dijit.form.TextBox({
			id : newpreid + "_prefix",
			"name" : newpreid + "_prefix",
			type : "text"
		}, paramPREHelper);
		dojo.create("br", null, param_inner);

		//suffix
		dojo.create("label", {
			"for" : newpreid + "_suffix",
			"class" : "innerparam_label",
			innerHTML : "suffix:"
		}, param_inner);
		var paramSUFFHelper = dojo.create("div", null, param_inner);
		new dijit.form.TextBox({
			id : newpreid + "_suffix",
			"name" : newpreid + "_suffix",
			type : "text"
		}, paramSUFFHelper);
		dojo.create("br", null, param_inner);

		//separator
		dojo.create("label", {
			"for" : newpreid + "_separator",
			"class" : "innerparam_label",
			innerHTML : "separator:"
		}, param_inner);
		var paramSEPHelper = dojo.create("div", null, param_inner);
		new dijit.form.TextBox({
			id : newpreid + "_separator",
			intermediateChanges : true,
			"name" : newpreid + "_separator",
			type : "text",
			onChange : function() {
				var last_ = this.id.lastIndexOf('_');
				var currentID = this.id.substring(0, last_);
//				var tabName = dijit.byId(currentID + "_separator").get('value');
				//check which radio box is active
				var children;
				if(dijit.byId(currentID + "_Input").get('checked')) {
					children = dijit.byId("input_tab_Container").getChildren();
					for(var i = 0; i < children.length; i++) {
						if(children[i].get('id') === currentID + "_Input_Tab") {
							if(this.value == "") {
								dijit.byId(currentID + "_minimum").set('value', 0);
								dijit.byId(currentID + "_minimum").constraints.min = 0;
								dijit.byId(currentID + "_minimum").constraints.max = 1;
								dijit.byId(currentID + "_maximum").set('value', 1);
								dijit.byId(currentID + "_maximum").set('disabled', true);
							} else {
								dijit.byId(currentID + "_minimum").set('value', 2);
								dijit.byId(currentID + "_minimum").constraints.max = 9999999;
								dijit.byId(currentID + "_minimum").set('disabled', false);
								dijit.byId(currentID + "_maximum").set('value', 2);
								dijit.byId(currentID + "_maximum").set('disabled', false);
							}
						}
					}
				}
				if(dijit.byId(currentID + "_Output").get('checked')) {
					children = dijit.byId("output_tab_Container").getChildren();
					for(var i = 0; i < children.length; i++) {
						if(children[i].get('id') === currentID + "_Output_Tab") {
							if(this.value == "") {
								dijit.byId(currentID + "_minimum").set('value', 0);
								dijit.byId(currentID + "_minimum").constraints.min = 0;
								dijit.byId(currentID + "_minimum").constraints.max = 1;
								dijit.byId(currentID + "_maximum").set('value', 1);
								dijit.byId(currentID + "_maximum").set('disabled', true);
							} else {
								dijit.byId(currentID + "_minimum").set('value', 2);
								dijit.byId(currentID + "_minimum").constraints.max = 9999999;
								dijit.byId(currentID + "_minimum").set('disabled', false);
								dijit.byId(currentID + "_maximum").set('value', 2);
								dijit.byId(currentID + "_maximum").set('disabled', false);
							}
						}
					}
				}
			}
		}, paramSEPHelper);

		//always add a <hr>
		if(i != num) {
			dojo.create("hr", {
				"class" : "innerparams_hr"
			}, param_inner);
		}
	}
}

function setRadioButtons(paramID, elementid) {
	dojo.create("input", {
		type : "radio",
		id : paramID + "_Input",
		value : "in"
	}, elementid);
	new dijit.form.RadioButton({
		onChange : function() {
			if(this.get('checked')) {
				var last_ = this.id.lastIndexOf('_');
				var currentID = this.id.substring(0, last_);
				dojo.byId(currentID + "_kind").value = "input";
				var tabName = dijit.byId(currentID + "_id").get('value');
				if(tabName === "") {
					last_ = currentID.lastIndexOf('-');
					var currentID_ = currentID.substring(last_, currentID.length);
					tabName = "ID" + currentID_;
					dijit.byId(currentID + "_id").set('value', tabName);
				}

				var newTab = true;
				var children = dijit.byId("output_tab_Container").getChildren();
				for(var i = 0; i < children.length; i++) {
					if(children[i].get('title') === tabName) {
						dijit.byId("output_tab_Container").removeChild(children[i]);
						dijit.byId("input_tab_Container").addChild(children[i]);
						newTab = false;
					}
				}
				if(newTab) {
					var newIn;
					//erzeuge neuen, wenn die is im output noch nicht vorhanden
					newIn = new dijit.layout.ContentPane({
						id : paramID + "_Input_Tab",
						title : tabName,
						content : createTabContent(currentID)
					});
					dijit.byId("input_tab_Container").addChild(newIn);
				}
				
				dojo.byId(currentID + "_minMaxDiv").style.visibility = 'visible';
			}
		},
		name : paramID
	}, paramID + "_Input");
	dojo.create("label", {
		"for" : paramID + "_Input",
		innerHTML : "Input"
	}, elementid);

	dojo.create("input", {
		type : "radio",
		id : paramID + "_Output",
		value : "out",
	}, elementid);
	new dijit.form.RadioButton({
		onChange : function() {
			if(this.get('checked')) {
				var last_ = this.id.lastIndexOf('_');
				var currentID = this.id.substring(0, last_);
				dojo.byId(currentID + "_kind").value = "output";
				var tabName = dijit.byId(currentID + "_id").get('value');
				if(tabName === "") {
					last_ = currentID.lastIndexOf('-');
					var currentID_ = currentID.substring(last_, currentID.length);
					tabName = "ID" + currentID_;
					dijit.byId(currentID + "_id").set('value', tabName);
				}

				var newTab = true;
				//lösche erst den alten tab aus dem input
				var children = dijit.byId("input_tab_Container").getChildren();
				for(var i = 0; i < children.length; i++) {
					if(children[i].get('title') === tabName) {
						dijit.byId("input_tab_Container").removeChild(children[i]);
						dijit.byId("output_tab_Container").addChild(children[i]);
						newTab = false;
					}
				}
				//erzeuge neuen tab
				if(newTab) {
					var newOut = new dijit.layout.ContentPane({
						id : paramID + "_Output_Tab",
						title : tabName,
						content : createTabContent(currentID)
					});
					dijit.byId("output_tab_Container").addChild(newOut);
				}

				//zeige minimum, maximum
				dojo.byId(currentID + "_minMaxDiv").style.visibility = 'hidden';
			}
		},
		name : paramID
	}, paramID + "_Output");
	dojo.create("label", {
		"for" : paramID + "_Output",
		innerHTML : "Output"
	}, elementid);
}

function clearInputOutputTab() {
	var children = dijit.byId("input_tab_Container").getChildren();
	if(children != undefined) {
		for(var i = 0; i < children.length; i++) {
			dojo.destroy(children[i]);
			dijit.byId("input_tab_Container").closeChild(children[i]);
		}
	}

	var children = dijit.byId("output_tab_Container").getChildren();
	if(children != undefined) {
		for(var i = 0; i < children.length; i++) {
			dojo.destroy(children[i]);
			dijit.byId("output_tab_Container").closeChild(children[i]);
		}
	}
}

function createTabContent(hid) {
	var tab_content = dojo.create("div");

	//title
	dojo.create("label", {
		"for" : hid + "_title",
		innerHTML : "Title:"

	}, tab_content);
	dojo.create("br", null, tab_content);
	var paramTITLEHelper = dojo.create("div", null, tab_content);
	new dijit.form.ValidationTextBox({
		id : hid + "_title",
		"name" : hid + "_title",
		type : "text",
		placeHolder : "the title of the parameter",
		required : true,
		style : {
			width : "300px"
		}
	}, paramTITLEHelper);
	dojo.create("br", null, tab_content);

	//abstract
	dojo.create("label", {
		"for" : hid + "_abstract",
		innerHTML : "Abstract:"

	}, tab_content);
	dojo.create("br", null, tab_content);
	var paramABSTRACTHelper = dojo.create("div", null, tab_content);
	new dijit.form.SimpleTextarea({
		id : hid + "_abstract",
		"name" : hid + "_abstract",
		value : "an abstract for the parameter",
		type : "text",
		placeHolder : "an abstract for the parameter",
		style : {
			"min-width" : "300px",
			"max-width" : "300px"
		}
	}, paramABSTRACTHelper);
	dojo.create("br", null, tab_content);

	//mimetype
	var currentID = hid + "_" + getId();

	dojo.create("label", {
		"for" : currentID + "_format",
		innerHTML : "Format:"
	}, tab_content);
	dojo.create("br", null, tab_content);
	var format_div_helper = dojo.create("div", null, tab_content);
	createFormatBox(currentID + "_format", format_div_helper);
	dojo.create("br", null, tab_content);

	dojo.create("div", {
		id : hid + "_formatadd"
	}, tab_content);
	dojo.create("img", {
		src : "images/add.png",
		width : "16",
		height : "16",
		alt : "Add",
		onClick : "addFormat(\'" + hid + "\')"
	}, tab_content);
	dojo.create("br", null, tab_content);

	var paramMINMAXDIVHelper = dojo.create("div", {
		id : hid + "_minMaxDiv"
	}, tab_content);

	//minimum
	dojo.create("label", {
		"for" : hid + "_minimum",
		innerHTML : "Minimum amount of arguments:"
	}, paramMINMAXDIVHelper);
	dojo.create("br", null, paramMINMAXDIVHelper);
	var paramMINIMUMHelper = dojo.create("div", null, paramMINMAXDIVHelper);
	new dijit.form.NumberSpinner({
		id : hid + "_minimum",
		"name" : hid + "_minimum",
		smallDelta : 1,
		intermediateChanges : true,
		constraints : {
			min : 0,
			max : 99999
		},
		style : {
			width : "40px"
		},
		onChange : function() {
			if(dijit.byId(hid + "_minimum").get('value') > dijit.byId(hid + "_maximum").get('value')) {
				dijit.byId(hid + "_maximum").set('value', dijit.byId(hid + "_minimum").get('value'));
			}
		}
	}, paramMINIMUMHelper);
	dojo.create("br", null, paramMINMAXDIVHelper);

	//maximum
	dojo.create("label", {
		"for" : hid + "_maximum",
		innerHTML : "Maximum amount of arguments:"

	}, paramMINMAXDIVHelper);
	dojo.create("br", null, paramMINMAXDIVHelper);
	var paramMAXIMUMHelper = dojo.create("div", null, paramMINMAXDIVHelper);
	new dijit.form.NumberSpinner({
		id : hid + "_maximum",
		"name" : hid + "_maximum",
		smallDelta : 1,
		intermediateChanges : true,
		value : 2,
		constraints : {
			min : 2
		},
		style : {
			width : "40px"
		},
		onChange : function() {
			if(dijit.byId(hid + "_maximum").get('value') < dijit.byId(hid + "_minimum").get('value')) {
				dijit.byId(hid + "_minimum").set('value', dijit.byId(hid + "_maximum").get('value'));
			}
		}
	}, paramMAXIMUMHelper);
	dojo.create("br", null, paramMINMAXDIVHelper);

	//maximum disablen, wenn kein separator vorhanden
	if(dijit.byId(hid + "_separator").get('value') === "") {
		dijit.byId(hid + "_minimum").set('value', 0);
		dijit.byId(hid + "_minimum").constraints.min = 0;
		dijit.byId(hid + "_minimum").constraints.max = 1;
		// dijit.byId(hid + "_minimum").set('disabled', true);
		dijit.byId(hid + "_maximum").set('value', 1);
		dijit.byId(hid + "_maximum").set('disabled', true);

	}
	return tab_content;
}

function createFormatBox(hid, place) {

	var mime_Types_Store = new dojo.data.ItemFileReadStore({
		url : "json/Mime_Types.json"
	});

	new dijit.form.FilteringSelect({
		id : hid,
		name : hid,
		value : "application/geotiff",
		store : mime_Types_Store,
		searchAttr : "mimetype",
		style : {
			width : "300px"
		}
	}, place);
}

function removeFormat(format) {
	dojo.destroy(format);
}

function addFormat(id) {
	var newId = id + "_" + getId();

	var format = dojo.create("div", {
		id : newId
	}, id + "_formatadd", "before");
	
	var format_add_helper_span = dojo.create("span");
	dojo.place(format_add_helper_span, format);
	
	var format_box = createFormatBox(newId + "_select", format_add_helper_span);
	var format_remove = dojo.create("img", {
		src : "images/del.png",
		width : "16",
		height : "16",
		alt : "Remove",
		onClick : "removeFormat(\'" + newId + "\')"
	}, format);
}
