dojo.addOnLoad(function() {

	//build and add label for container type
	var contype_label = dojo.create("label", {
		"for" : "containertype",
		innerHTML : "Container Type:"
	});
	dojo.place(contype_label, "Cont_Type");
	dojo.create("br", null, "Cont_Type");

	var contype_box = dojo.create("input", {
		id : "cont_type_select"
	});
	dojo.place(contype_box, "Cont_Type");

	var container_Types_Store = new dojo.data.ItemFileReadStore({
		url : "json/Container_Types.json"
	});

	new dijit.form.FilteringSelect({
		id : "cont_type_select",
		name : "cont_type_select",
		value: "urn:n52:container:python:2.5",
		store : container_Types_Store,
		searchAttr : "container",
		style : {
			width : "350px"
		}
	}, "cont_type_select");
});
