dojo.require("dojox.form.Uploader");

dojo.addOnLoad(function() {
	new dojox.form.Uploader({
		id : "executable",
		label : "select file",
		multiple : false,
		onChange : function() {
			dojo.byId("selected_file").innerHTML = "selected File: <b>"+this.getFileList()[0].name;
		}
	}, "executable_selector");
});
