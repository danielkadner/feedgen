includeCSS("css/feed.css");
includeCSS("css/RunEnvTab.css");
includeCSS("css/InOutTab.css");

// includeJS("js/scripts/RadioButtons.js");
includeJS("js/scripts/ContType.js");
includeJS("js/scripts/RunEnv.js");
includeJS("js/scripts/SelectButton.js");
includeJS("js/scripts/ParamSetting.js");


dojo.require("dijit.Dialog");
dojo.require("dijit.form.Select");
dojo.require("dijit.form.Button");
dojo.require("dijit.form.TextBox");
dojo.require("dijit.form.ValidationTextBox");
dojo.require("dijit.form.Textarea");
dojo.require("dijit.form.SimpleTextarea");
dojo.require("dijit.form.Form");
dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dojo.data.ItemFileReadStore");

dojo.require("dijit.form.ComboBox");
dojo.require("dijit.Dialog");

dojo.require("dijit.form.FilteringSelect");

dojo.require("dijit.form.CheckBox");
dojo.require("dijit.form.NumberTextBox");

dojo.require("dijit.form.NumberSpinner");

dojo.addOnLoad(function() {
	
});
function getId() {
	var id = document.getElementById("id").value;
	var newId = (id - 1) + 2;
	document.getElementById("id").value = newId;
	return newId;
}

function includeJS(filename) {
	var head = document.getElementsByTagName('head')[0];
	var script = document.createElement('script');
	script.src = filename;
	script.type = 'text/javascript';
	head.appendChild(script)
}

function includeCSS(filename) {
	var head = document.getElementsByTagName('head')[0];
	var link = document.createElement('link');
	link.rel = 'stylesheet';
	link.type = 'text/css';
	link.href = filename;
	head.appendChild(link)
}