<%@page import="feed.FeedGenerator"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import=" org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Feed Package Generator</title>
<jsp:scriptlet><![CDATA[//parametrized calling:
			if (request != null) {
				FeedGenerator fg = new FeedGenerator(request);
				if (fg.isCreated()) {
					File zip = fg.getZipFile();
					ServletOutputStream sos = null;
					FileInputStream stream;
					try {
						response.setContentType("application/xml");
						response.addHeader("Content-Disposition", "attachment; filename=" + zip.getName());
						sos = response.getOutputStream();
						stream = new FileInputStream(zip);
						BufferedInputStream bis = new BufferedInputStream(stream);
						int readBytes = 0;
						while ((readBytes = bis.read()) != -1)
							sos.write(readBytes); 
						if (bis != null)
							bis.close();
						if (stream != null)
							stream.close();
						out.clear();
						out = pageContext.pushBody();
					} finally {
						if (sos != null)
							sos.close();
					}
				}
			} 
			]]></jsp:scriptlet>
</head>
<body>
	<jsp:include page="/index_feed.html" />
</body>
</html>